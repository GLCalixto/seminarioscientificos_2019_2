package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.List;

public class Estudante {

    private Long id;

    private String nome;

    private String telefone;

    private String email;

    private List<Inscricao> inscricoes = new ArrayList<>();

    private Instituicao instituicao;

    public Estudante(Instituicao inst) {

        this.instituicao = inst;

    }

    public void adicionarInscricao(Inscricao ins) {

        this.inscricoes.add(ins);

    }

    public boolean possuiInscricao(Inscricao ins) {

        return this.inscricoes.contains(ins);
    }

    public void removerInscricao(Inscricao ins) {

        if (this.inscricoes.contains(ins)) {

            this.inscricoes.remove(ins);

        }

    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return this.telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public Instituicao getInstituicao() {
        return this.instituicao;
    }
}
