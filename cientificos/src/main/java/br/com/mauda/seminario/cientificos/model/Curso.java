package br.com.mauda.seminario.cientificos.model;

public class Curso {

    private int id;

    private String nome;

    private AreaCientifica area;

    public Curso(AreaCientifica a) {
        this.area = a;
        this.area.adicionarCurso(this);
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public AreaCientifica getAreaCientifica() {
        return this.area;
    }
}
