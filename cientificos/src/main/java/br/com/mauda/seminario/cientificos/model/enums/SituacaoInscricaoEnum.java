package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {

    DISPONIVEL(1L, "disponivel"),
    COMPRADO(2L, "Comprado"),
    CHECKIN(3L, "Checkin");

    private Long id;
    private String nome;

    private SituacaoInscricaoEnum(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return this.id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    protected void setNome(String nome) {
        this.nome = nome;
    }

    public static SituacaoInscricaoEnum getBy(String nome) {
        switch (nome) {
            case "DISPONIVEL":
                return DISPONIVEL;
            case "COMPRADO":
                return COMPRADO;
            case "CHECKIN":
                return CHECKIN;
            default:
                return null;

        }
    }
}
