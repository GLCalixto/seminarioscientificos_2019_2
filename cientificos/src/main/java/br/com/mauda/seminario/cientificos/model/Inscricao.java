package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;

    private Boolean direitoMaterial;

    private SituacaoInscricaoEnum situacao;

    private Seminario seminario;

    private Estudante estudante;

    public Inscricao(Seminario semi) {

        this.seminario = semi;

        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;

    }

    public void cancelarCompra() {

        this.situacao = SituacaoInscricaoEnum.DISPONIVEL;

        this.estudante.removerInscricao(this);

        this.direitoMaterial = null;

        this.estudante = null;

    }

    public void comprar(Estudante est, Boolean direitoMaterial) {

        this.situacao = SituacaoInscricaoEnum.COMPRADO;

        this.direitoMaterial = direitoMaterial;

        this.estudante = est;

        this.estudante.adicionarInscricao(this);
    }

    public void realizarCheckIn() {

        this.situacao = SituacaoInscricaoEnum.CHECKIN;

    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }
}
