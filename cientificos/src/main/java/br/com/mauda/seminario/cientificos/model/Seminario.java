package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seminario {

    private int id;

    private String titulo;

    private String descricao;

    private Boolean mesaRedonda;

    private Date data;

    private Integer qtdInscricoes;

    private List<AreaCientifica> areasCientificas = new ArrayList<>();

    private List<Inscricao> inscricoes = new ArrayList<>();

    private List<Professor> professores = new ArrayList<>();

    public Seminario(AreaCientifica area, Professor prof, Integer qtd) {

        this.areasCientificas.add(area);

        this.professores.add(prof);

        prof.adicionarSeminario(this);

        this.qtdInscricoes = qtd;

        for (int i = 0; i < this.qtdInscricoes; i++) {
            this.getInscricoes().add(new Inscricao(this));
        }

    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setQtdIncricoes(Integer qtdIncricoes) {
        this.qtdInscricoes = qtdIncricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }
}
